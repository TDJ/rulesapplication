import datetime
import typing

import telegram

from question_catalogue import QuestionCatalogue
from quiz import Quiz

# settings for default test
TEST_QUESTION_COUNT = 25
MAX_ERRORS = 7
MAX_ERRORS_PERCENTAGE = MAX_ERRORS / TEST_QUESTION_COUNT


class Game:

    def __init__(self, mode: str, question_catalogue: QuestionCatalogue, questions_count: int = TEST_QUESTION_COUNT, max_error_percentage: float = MAX_ERRORS_PERCENTAGE):
        """Creates a new game with the given question catalogue and starts the timer for this game.

        Args:
            mode (str): Either "test" or "exercise"
            question_catalogue (QuestionCatalogue): The questions to be used in this game
            questions_count (int, optional): The number of questions to ask. Ignored if mode is "exercise". Defaults to TEST_QUESTION_COUNT.
            max_error_percentage (float, optional): Maximum percentage of wrongly answered questions to pass. Defaults to MAX_ERRORS_PERCENTAGE.
        Raises:
            ValueError: If the mode is not either "test" or "exercise".
        """
        self.mode = mode
        self.question_catalogue = question_catalogue
        if self.mode == "test":
            self.quiz = Quiz(self.question_catalogue, questions_count)
        elif self.mode == "exercise":
            self.quiz = Quiz(self.question_catalogue)
        else:
            raise ValueError
        self.timer = datetime.datetime.now().replace(microsecond=0)
        self.max_error_percentage = max_error_percentage

    def finished(self) -> bool:
        """Check whether this game is finished.

        Returns:
            bool: True if finished, False otherwise.
        """
        return self.quiz.finished()

    def time_elapsed(self) -> datetime.timedelta:
        """Calculates time passed from start/creation of the game until now.

        Returns:
            timedelta: Time passed since start of the game
        """
        return datetime.datetime.now().replace(microsecond=0) - self.timer

    def show_question(self) -> str:
        """Returns the next question formatted as markdown string containing question number and the question text.

        Returns:
            str: The formatted question string
        """
        question = self.quiz.get_next_question()

        text = (f"❓ *Frage {self.quiz.get_current_question_number()}/{self.quiz.question_count()} "
                f"\({question.full_number(True)}\)*:\n"
                f"{telegram.helpers.escape_markdown(question.question_text, 2)}")
        return (text, question.path)

    def add_answer(self, answer: bool) -> None:
        """Adds the users answer for the current question.

        Args:
            answer (bool): The users answer
        """
        self.quiz.add_answer(answer)

    def check_answer(self) -> typing.List[str]:
        """Stores users answer,  returns feedback and the correct reasoning. Only used in exercise mode.

        Returns:
            [str]: List of markdown formatted strings
        """
        if self.mode == "exercise":
            message = [
                self.quiz.check_answer(),
                self.quiz.get_current_question().reason_string()
            ]
            return message

    def result(self) -> typing.List[str]:
        """Returns feedback and evaluation of the current game. Does nothing when in test mode and the game is not finished yet.
            Contains all wrongly answered questions and the correct reasoning in test mode.
        Returns:
            [str]: Array of markdown formatted strings containing error percentages, evaluation and time elapsed.
        """
        if self.mode == "test" and (not self.quiz.finished()):
            return

        result = [(f"📊 *Auswertung* \n"
                   f"_Fehler_: {self.quiz.get_wrong_answer_count()} von {self.quiz.question_count()}"
                   f"\({self.quiz.get_wrong_percentage_string(True)}\) \n"
                   f"_Ergebnis_: {self.quiz.result(self.max_error_percentage)} \n"
                   f"_Benötigte Zeit_: {self.time_elapsed()}⏲️")
                  ]
        questions = []

        if self.mode == "test":
            result.append("Folgende Fragen wurden _falsch_ beantwortet:")
            if self.quiz.get_wrong_answers():
                # add wrong questions and their reason
                questions = [question['question']
                             for question in self.quiz.get_wrong_answers()]
            else:
                # no wrong answer, congratulations!
                result.append("_🎉 keine 🎉_")
        return (result, questions)
