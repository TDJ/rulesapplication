import logging
import os
from pathlib import Path

import telegram
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import (ApplicationBuilder,
                          CallbackQueryHandler,
                          CommandHandler,
                          ContextTypes, MessageHandler, filters)

from game import Game
from question_catalogue import QuestionCatalogue

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
# read bot api token from file
TOKEN_FILE = os.path.join(__location__, 'token.txt')
TOKEN = Path(TOKEN_FILE).read_text().strip()

TEST_QUESTION_COUNT = 25
MAX_ERRORS = 7
MAX_ERRORS_PERCENTAGE = MAX_ERRORS / TEST_QUESTION_COUNT


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Called when starting the bot or when finishing a game. Shows short "How To".

    Args:
        update (Update): Telegram Update
        context (CallbackContext): Telegram Context
    """
    global catalogue
    await context.bot.send_message(
        text="🏀 *Willkommen zum Basketball Regelquiz Bot*\n"
        + "Dieser Bot basiert auf dem aktuellen Regel\- und Kampfrichterfragenkatalog des DBB "
        + f"\({catalogue.catalogue_name(True)}\)\ "
        + f"mit insgesamt {catalogue.question_count()} Fragen\. \n"
        + "Hier erklären wir dir kurz wie\'s geht: \n"
        + "Nutze den _Übungsmodus_ um dir einzelne Fragen anzeigen zu lassen\. "
        + "Nach jeder Frage siehst du direkt die richtige Antwort und Begründung\. \n"
        + f"Beim _Regeltest_ bekommst du {TEST_QUESTION_COUNT} Fragen gestellt, von denen du maximal {MAX_ERRORS} falsch beantworten darfst\.",
        chat_id=update.effective_chat.id,
        parse_mode='MarkdownV2',
        reply_markup=telegram.ReplyKeyboardRemove()
    )

    keyboard = [
        [InlineKeyboardButton("📚 Übungsmodus", callback_data='exercise')],
        [InlineKeyboardButton("📝 Regeltest", callback_data='test')],
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    await update.message.reply_text(
        'Bitte wähle den gewünschten Modus aus:', reply_markup=reply_markup)


async def ask_question(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Displays the next question from the game and displays the ReplyKeyboard.

    Args:
        update (Update): Telegram Update
        context (CallbackContext): Telegram Context
    """
    if 'game' not in context.user_data:
        help_command(update, context)
        return
    try:
        question_data = context.user_data["game"].show_question()
    except IndexError:
        # no more questions available
        await context.bot.send_message(chat_id=update.effective_chat.id,
                                       text="Du hast alle verfügbaren Fragen in diesem Durchlauf bereits beantwortet\.",
                                       parse_mode=telegram.constants.ParseMode.MARKDOWN_V2)
        await show_result(update, context)
        return

    reply_markup = telegram.ReplyKeyboardMarkup([['Ja'], ['Nein']], True)

    await context.bot.send_message(chat_id=update.effective_chat.id,
                                   text=question_data[0],
                                   parse_mode=telegram.constants.ParseMode.MARKDOWN_V2,
                                   reply_markup=reply_markup)

    if question_data[1] is not None:
        # question is image question
        await context.bot.sendPhoto(chat_id=update.effective_chat.id,
                                    photo=open(question_data[1], 'rb'),
                                    parse_mode=telegram.constants.ParseMode.MARKDOWN_V2)


async def show_result(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Shows result of game to user including the evaluation. Presents options to play again

    Args:
        update (Update): Telegram Update
        context (CallbackContext): Telegram Context
    """
    if 'game' not in context.user_data:
        await help_command(update, context)
        return
    if not context.user_data["game"].finished():
        # only show results if game is finished
        return

    result = context.user_data["game"].result()
    result_texts, questions = result

    reply_markup = telegram.ReplyKeyboardMarkup([['/start']], True)

    for text in result_texts:
        await context.bot.send_message(chat_id=update.effective_chat.id,
                                       text=text,
                                       parse_mode=telegram.constants.ParseMode.MARKDOWN_V2,
                                       reply_markup=reply_markup)
        reply_markup = None
    # show wrong answers and their reason
    for question in questions:
        if question.image_question():
            # question is image question, show question number before image
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=question.question_header(),
                                           parse_mode=telegram.constants.ParseMode.MARKDOWN_V2)
            # reason as caption below image
            await context.bot.sendPhoto(chat_id=update.effective_chat.id,
                                        photo=open(question.path, 'rb'),
                                        caption=question.reason_string(),
                                        parse_mode=telegram.constants.ParseMode.MARKDOWN_V2)
        else:
            # one message containing all
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=question.full_question_string(),
                                           parse_mode=telegram.constants.ParseMode.MARKDOWN_V2)

    await context.bot.send_message(chat_id=update.effective_chat.id,
                                   text="🔁 Benutze /start um ein neues Quiz zu starten\.",
                                   parse_mode=telegram.constants.ParseMode.MARKDOWN_V2,
                                   reply_markup=reply_markup)


async def button(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Handles inline keyboard presses.

    Args:
                update (Update): Telegram Update
        context (CallbackContext): Telegram Context
    """
    query = update.callback_query

    # CallbackQueries need to be answered, even if no notification to the user is needed
    # Some clients may have trouble otherwise. See https://core.telegram.org/bots/api#callbackquery
    await query.answer()

    global catalogue

    if query.data == "exercise":
        await query.edit_message_text(text="📚 Übungsmodus ausgewählt")
        context.user_data["game"] = Game(query.data, catalogue)
        await ask_question(update, context)
    elif query.data == "test":
        await query.edit_message_text(text="📝 Testmodus ausgewählt")
        context.user_data["game"] = Game(
            query.data, catalogue, TEST_QUESTION_COUNT, MAX_ERRORS_PERCENTAGE)
        await ask_question(update, context)
    elif query.data == "next":
        await query.edit_message_text(text="⏭️ Nächste Frage:")
        await ask_question(update, context)
    elif query.data == "result":
        await query.edit_message_text(text="📚 Übungsmodus beendet")
        await show_result(update, context)


async def exercise_options_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Removes inline keyboard and proceeds with next question.

    Args:
        update (Update): Telegram Update
        context (CallbackContext): Telegram Context
    """
    if context.match.group(0).lower() == "nächste frage":
        await ask_question(update, context)
    else:
        await show_result(update, context)


async def answer_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Handles user answers based on chat message. Shows inline keyboards for next options or shows result.

    Args:
        update (Update): Telegram Update
        context (CallbackContext): Telegram Context
    """
    if 'game' not in context.user_data:
        await help_command(update, context)
        return

    if context.match.group(0).lower() == "ja":
        context.user_data["game"].add_answer(True)
    else:
        context.user_data["game"].add_answer(False)

    if context.user_data["game"].mode == "exercise":
        # show answer validation
        for text in context.user_data["game"].check_answer():
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=text,
                                           parse_mode=telegram.constants.ParseMode.MARKDOWN_V2,
                                           reply_markup=telegram.ReplyKeyboardMarkup(
                                               [['Nächste Frage'], [
                                                   'Auswertung']], True
                                           ))

    if context.user_data["game"].mode == "exercise":
        keyboard = [
            [InlineKeyboardButton("⏭️ Nächste Frage",
                                  callback_data='next')],
            [InlineKeyboardButton("📊 Auswertung", callback_data='result')],
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        await update.message.reply_text(
            'Wie möchtest du fortfahren:', reply_markup=reply_markup)
    else:
        if not context.user_data["game"].finished():
            await ask_question(update, context)
        else:
            await show_result(update, context)


async def help_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Displays help message if a command is unknown

    Args:
        update (Update): Telegram Update
        context (CallbackContext): Telegram Context
    """
    await update.message.reply_text("Benutze /start um den Bot zu starten.")


def main():
    """Creates question catalogue and starts bot
    """
    global catalogue

    catalogue = QuestionCatalogue(2024, 1)
    catalogue.add_questions_from_file(
        os.path.join(__location__, '../resources/rules.csv'))
    catalogue.add_questions_from_file(
        os.path.join(__location__, '../resources/table.csv'))
    catalogue.add_questions_from_file(
        os.path.join(__location__, '../resources/image-questions.csv'), True)

    application = ApplicationBuilder().token(TOKEN).build()

    application.add_handler(CommandHandler('start', start))
    application.add_handler(CallbackQueryHandler(button))
    application.add_handler(MessageHandler(
        filters.Regex("^(?:ja|Ja|nein|Nein)$"),
        answer_handler
    ))
    application.add_handler(MessageHandler(
        filters.Regex("^(?:Nächste Frage|Auswertung)$"),
        exercise_options_handler
    ))
    application.add_handler(CommandHandler('help', help_command))

    application.run_polling()


if __name__ == '__main__':
    main()
