import telegram


class Question:

    def __init__(self, question_type: str, number: int, question_text: str, reason_text: str, article: str, answer: bool, path: str = None):
        """Creates a new question object

        Args:
            question_type (str): Type of the question, mostly "R" for rule questions or "K" for questions for table officials.
            number (int): The number of the question
            question_text (str): The question itself
            reason_text (str): The reasoning for the correct answer
            article (str): The article or interpretation on which the question is based
            answer (bool): The correct answer
            path (str, optional): The path to an image, if the question has one. Defaults to None.
        """
        self.question_type = question_type
        self.number = number
        self.question_text = question_text
        self.reason_text = reason_text
        self.article = article
        self.answer = answer
        self.path = path

    def image_question(self) -> bool:
        """Determines whether this question has an image and is therefore an image question or not.

        Returns:
            bool: True, if the question is an image question. False otherwise.
        """
        return self.path is not None

    def full_number(self, escape_markdown: bool = False) -> str:
        """Returns the complete question number, composed of the question type and the question number

        Args:
            escape_markdown (bool, optional): Escape the returned string for use in markdown. Defaults to False.

        Returns:
            str: Complete question number, e.g. "R-42" or "K-9"
        """
        text = self.question_type + "-" + self.number
        if escape_markdown:
            return telegram.helpers.escape_markdown(text, 2)
        else:
            return text

    def question_header(self) -> str:
        """Returns markdown formatted header for questions containing the full question number only

        Returns:
            str: Header string , markdown escaped
        """
        return f"*❓ Frage {telegram.helpers.escape_markdown(self.full_number(), 2)}*: \n"

    def full_question_string(self) -> str:
        """Returns markdown escaped string containing question number, question text and reason

        Returns:
            str: The resulting question, markdown escaped
        """
        return (f"{self.question_header()}"
                f"{telegram.helpers.escape_markdown(self.question_text, 2)}\n"
                f"{self.reason_string()}")

    def reason_string(self) -> str:
        """Returns markdown escaped reasoning string

        Returns:
            str: Reason string, markdown escaped
        """
        return (f"ℹ️ *Begründung*: \n"
                f"{telegram.helpers.escape_markdown(self.reason_text, 2)}\n")
