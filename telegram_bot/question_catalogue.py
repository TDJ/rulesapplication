import csv
import os
import typing

import telegram

from question import Question


class QuestionCatalogue:

    def __init__(self, year: int, version: int, questions: typing.List[Question] = []):
        """Creates a new question catalogue, containing zero ore more questions

        Args:
            year (int): The year of publication for this catalogue
            version (int): The version within the publication year
            questions ([Question], optional): Questions to initialize the catalogue with. Defaults to [].
        """
        self.questions = questions
        self.version = version
        self.year = year

    def catalogue_name(self, escape_markdown: bool = False) -> str:
        """The name of the catalogue, composed of year and version, e.g. "2020_V2"

        Args:
            escape_markdown (bool, optional): Escape the resulting name for markdown use. Defaults to False.

        Returns:
            str: The name of the catalogue
        """
        name = str(self.year)
        if self.version != 1:
            name = f"{self.year}_V{self.version}"

        if escape_markdown:
            return telegram.helpers.escape_markdown(name, 2)
        else:
            return name

    def question_count(self) -> int:
        """Returns number of questions in this catalogue

        Returns:
            int: Number of questions
        """
        return len(self.questions)

    def add_questions_from_file(self, filename: str, image_mode: bool = False):
        """Import questions from a csv-formatted file.
        Lines must be formatted according to the following format: "Nr.;Frage;J;N;Antwort;Art.;"
        If image_mode is used the format is as follows: "Nr.;Frage;J;N;Antwort;Art.;Path;"
        Args:
            filename (str): The csv file to read from. Uses ";" as delimiter.
            image_mode (bool, optional): The input file contains image questions, additional path column to read. Defaults to False.
        """
        with open(filename, newline='', encoding="utf-8-sig") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';')
            for row in reader:
                if row['J'].lower() == 'x':
                    answer = True
                else:
                    answer = False
                if not ('K-' in row['Nr.'] or 'R-' in row['Nr.']):
                    break
                # extract question type and number
                parts = row['Nr.'].split('-')
                question_type = parts[0]
                number = parts[1]
                path = None
                if image_mode:
                    # construct full path to image
                    path = os.path.join(os.path.dirname(filename), row['Path'])
                # create question
                self.questions.append(Question(question_type, number,
                                               row['Frage'], row['Antwort'], row['Art.'], answer, path))
