import random
import typing

import telegram

from question import Question
from question_catalogue import QuestionCatalogue


class Quiz:

    def __init__(self, question_catalogue: QuestionCatalogue, question_count: int = 0):
        """Creates a new quiz based on the given question catalogue

        Args:
            question_catalogue (QuestionCatalogue): Questions to randomly choose from.
            question_count (int, optional): Populate the quiz with question_count questions. Must be greater or equal to 0.
                If 0 is given, "exercise mode" is used.
                This mode always appends new questions to the quiz when the next question is asked. Defaults to 0.

        Raises:
            ValueError: If the question_count is negative.
        """
        self.question_catalogue = question_catalogue
        if question_count < 0:
            raise ValueError
        # do not select more questions than available
        self.questions = [{"question": question, "answer": None} for question in random.sample(
            self.question_catalogue.questions, min(question_count, question_catalogue.question_count()))]
        self.current_question_index = -1

    def question_count(self) -> int:
        """Current number of questions int this quiz.

        Returns:
            int: Number of questions
        """
        return len(self.questions)

    def get_current_question(self) -> Question:
        """Returns the active question. This is usually the first unanswered question.

        Returns:
            Question: The current active question
        """
        return self.questions[self.current_question_index]['question']

    def check_answer(self, question: Question = None) -> str:
        """Checks and stores the answer for the current active question or a specified question.

        Args:
            question (Question, optional): Question to check the answer for. If None is given, the current question is assumed. Defaults to None.

        Returns:
            str: String containing feedback if the question was answered correctly.
        """
        index = self.current_question_index
        if question is not None:
            index = self.questions.index(
                {"question": question, "answer": None})
        if self.questions[index]['answer'] == self.questions[index]['question'].answer:
            return "✅ Richtig\! 🎉"
        else:
            return "❌ Leider falsch 😔"

    def get_current_question_number(self) -> int:
        """Returns the number of the question in this quiz. 1-based indexed.

        Returns:
            int: The 1-based index of the current active question
        """
        return self.current_question_index + 1

    def finished(self) -> bool:
        """Returns whether this quiz is finished (there are no unanswered questions) or not.

        Returns:
            bool: True if quiz is finished. False otherwise.
        """
        # finished if there are no unanswered questions
        return len(list(filter(lambda el: (el['answer'] is None), self.questions))) == 0

    def add_unique_question(self) -> None:
        """Appends a unique question from the catalogue to the quiz.
        Raises:
            IndexError: If the quiz already contains all questions from the catalogue
        """
        current_questions = set(x['question'] for x in self.questions)
        # calculate difference of the two lists to avoid adding duplicates
        available_questions = list(
            set(self.question_catalogue.questions) - current_questions)
        if available_questions:
            self.questions.append(
                {"question": random.choice(available_questions), "answer": None})

    def get_next_question(self) -> Question:
        """Returns the next question of the quiz.
        Sets the active question accordingly.
        If the quiz is already finished (usually in exercise mode) a new question is appended and then returned.

        Returns:
            Question: The new current active question.
        Raises:
            IndexError: If the quiz already contains all questions from the catalogue
        """
        if self.finished():
            self.add_unique_question()

        if self.current_question_index == -1 or self.questions[self.current_question_index]['answer'] is not None:
            # only go to next question when user answered the current one
            self.current_question_index += 1
        return self.questions[self.current_question_index]['question']

    def add_answer(self, answer: bool, question: Question = None) -> None:
        """Stores an answer for the given question.

        Args:
            answer (bool): The user submitted answer to the question
            question (Question, optional): The question the answer was submitted for. If None is given, the current active question is assumed. Defaults to None.
        """
        index = self.current_question_index
        if question is not None:
            index = self.questions.index(
                {"question": question, "answer": None})
        if self.questions[index]['answer'] is None:
            self.questions[index]['answer'] = answer

    def get_wrong_answers(self) -> typing.List:
        """Returns all wrongly answered questions as a list.

        Returns:
            list: All questions where the users answer does not match the correct answer
        """
        return list(filter(lambda el: (el['answer'] != el['question'].answer), self.questions))

    def get_wrong_answer_count(self) -> int:
        """Returns the number of wrongly answered questions

        Returns:
            int: Number of questions where the users answer does not match the correct answer
        """
        return len(self.get_wrong_answers())

    def get_wrong_percentage(self) -> float:
        """Computes the percentage of wrong answers based on total question count in this quiz

        Returns:
            float: The percentage of wrongly answered questions
        """
        return self.get_wrong_answer_count() / self.question_count()

    def get_wrong_percentage_string(self, escape_markdown: bool = False) -> str:
        """Formates the percentage as string with two decimals precision, e.g. "66.67%".

        Args:
            escape_markdown (bool, optional): Escape the returned string for use in markdown. Defaults to False.

        Returns:
            str: Formatted percentage string
        """
        percentage = f"{round(self.get_wrong_percentage() * 100, 2)}%"
        if escape_markdown:
            return telegram.helpers.escape_markdown(percentage, 2)
        else:
            return percentage

    def result(self, max_error_percentage: float) -> str:
        """Returns the result (passed or failed) of the quiz as string depending on the allowed error percentage.

        Args:
            max_error_percentage (float): The maximum percentage of wrongly answered questions allowed to pass this quiz.

        Returns:
            str: Result of the as string
        """
        if self.get_wrong_percentage() <= max_error_percentage:
            return "bestanden ✅"
        else:
            return "NICHT BESTANDEN ❌"
