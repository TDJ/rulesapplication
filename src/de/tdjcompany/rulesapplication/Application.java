package de.tdjcompany.rulesapplication;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Application {

    public static final int QUESTION_COUNT = 21;
    public static final int MAX_ERRORS = 6;
    private Scanner scanner;
    private List<Question> questions;

    public Application() {
        questions = new LinkedList<>();

        this.createRules("rules.csv");
        this.createRules("table.csv");

        this.startQuiz();
    }

    private void createRules(String path) {
        System.out.println("Reading input file: " + path);
        Scanner scanner;
        try {
            scanner = new Scanner(this.getClass().getClassLoader().getResourceAsStream(path), StandardCharsets.UTF_8.name());
        } catch (NullPointerException e) {
            e.printStackTrace();
            return;
        }

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] values = line.split(";");
            if (line.contains("R-") || line.contains("K-")) {
                String[] typeNumber = values[0].split("-");
                String type = typeNumber[0].trim();
                int number = Integer.parseInt(typeNumber[1].trim());
                String questionText = values[1];
                boolean answer = false;
                if (values[2].equals("x")) {
                    answer = true;
                }
                String reasonText = values[4];
                String article = values[5];
                Question question = new Question(type, number, answer, questionText, reasonText, article);
                questions.add(question);
            }
        }
        scanner.close();
    }

    private void startQuiz() {
        scanner = new Scanner(System.in);

        Collections.shuffle(questions);
        System.out.println("Generating quiz out of " + questions.size() + " questions...");
        List<Question> quizQuestions = questions.subList(0, QUESTION_COUNT);
        boolean[] userAnswers = new boolean[quizQuestions.size()];
        long startTime = System.currentTimeMillis();
        System.out.println("----------------------------------------------------------------------");
        System.out.println(" REGELTEST, max. " + MAX_ERRORS + " Fehler bei " + QUESTION_COUNT + " Fragen");
        System.out.println("----------------------------------------------------------------------");
        for (int i = 0; i < quizQuestions.size(); i++) {
            Question quizQuestion = quizQuestions.get(i);
            userAnswers[i] = this.askQuestion(quizQuestion);
        }
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        this.evaluateQuiz(userAnswers, quizQuestions, elapsedTime);
    }

    private boolean askQuestion(Question question) {

        System.out.println(question.getFullNumber() + ": " + question.getQuestionText());

        String userAnswer = "";
        while (!(userAnswer.equalsIgnoreCase("j") || userAnswer.equalsIgnoreCase("Ja")
                || userAnswer.equalsIgnoreCase("n") || userAnswer.equalsIgnoreCase("Nein"))) {
            System.out.println("Antwort (j/n): ");
            userAnswer = scanner.next();
        }

        return userAnswer.equalsIgnoreCase("j") || userAnswer.equalsIgnoreCase("Ja");
    }

    private void evaluateQuiz(boolean[] userAnswers, List<Question> quizQuestions, long time) {
        int errorCount = 0;
        System.out.println("----------------------------------------------------------------------");
        System.out.println(" AUSWERTUNG");
        System.out.println("----------------------------------------------------------------------");
        for (int i = 0; i < userAnswers.length; i++) {
            Question q = quizQuestions.get(i);
            if (userAnswers[i] == q.getAnswer()) {
                System.out.println(q.getFullNumber() + ": richtig beantwortet.");
            } else {
                errorCount++;
                System.out.println(q.getFullNumber() + ": falsch beantwortet.");
                System.out.println("------");
                System.out.println("Frage: " + q.getQuestionText());
                System.out.println("Richtige Antwort: " + (q.getAnswer() ? "richtig" : "falsch"));
                System.out.println("Begründung: " + q.getReasonText());
                System.out.println("------");
            }
        }
        System.out.println("Im Test wurden " + errorCount + " von " + QUESTION_COUNT + " (" +
                Math.round(((double) errorCount / QUESTION_COUNT) * 100)
                + "%) Fragen falsch beantwortet.");
        int seconds = (int) (time / 1000) % 60;
        int minutes = (int) ((time / (1000 * 60)) % 60);
        System.out.println("Die Bearbeitung dauerte " + minutes + "m " + seconds + "s.");
        System.out.print("Testergebnis: ");
        if (errorCount > MAX_ERRORS) {
            System.out.println("NICHT BESTANDEN");
        } else {
            System.out.println("bestanden");
        }
        System.out.println("----------------------------------------------------------------------");
        System.out.println("Zum Schließen beliebige Taste drücken.");
        System.out.println("Zum erneuten Testen Programm neustarten.");
        System.out.println("----------------------------------------------------------------------");
    }
}
