package de.tdjcompany.rulesapplication;

public class Question {

    private String type;
    private int number;
    private boolean answer;
    private String questionText;
    private String reasonText;
    private String article;

    public Question(String type, int number, boolean answer, String questionText, String reasonText, String article) {
        this.type = type;
        this.number = number;
        this.answer = answer;
        this.questionText = questionText;
        this.reasonText = reasonText;
        this.article = article;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean getAnswer() {
        return answer;
    }

    public void setAnswer(boolean answer) {
        this.answer = answer;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getReasonText() {
        return reasonText;
    }

    public void setReasonText(String reasonText) {
        this.reasonText = reasonText;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getFullNumber() {
        return this.type + "-" + this.getNumber();
    }
}
